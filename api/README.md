# Költözzbe API-n keresztüli betöltés

Minden URL az API elérési útjával és verziószámával kezdődik, azaz így: `https://api.koltozzbe.hu/v1/`.

```shell
curl -u agency_key:x -H 'User-Agent: KliensNeve (neved@valami.hu)' https://api.koltozzbe.hu/v1/listings.xml
```

Ha létrehozni szeretnél egy rekordot, akkor a kérést ki kell egészítened a szerializált adattal:

```shell
curl -u agency_key:x \
  -H 'User-Agent: KliensNeve (neved@valami.hu)' \
  -d '<listing> ... </listing>' \
  https://api.koltozzbe.hu/v1/listing/1.xml
```

## Authentikáció

A betöltéshez szükséged lesz az irodádhoz tartozó iroda-kulcsra (agency_key), amit irodavezetőként belépve tudsz lekérdezni. Ezzel azonosítod az irodádat. Ennek a birtokában bárki tud hirdetést feladni vagy hirdetést törölni a nevedben, úgyhogy vigyázz rá.

Az API HTTP Basic authentikációt használ, ami tökéletesen biztonságos, hiszen az API-t csak SSL-en keresztül érheted el.

Az API hívásoknál az agency_key-t kell felhasználónévként megadnod, jelszónak pedig bármit átadhatsz (a mi példáinkban: "x").

## Kliensed is azonosítsd

Minden kérés fejlécének tartalmaznia kell a `User-Agent` mezőben a kliensed nevét és az elérhetőségedet. Például:

    User-Agent: Valamilyen Ingatlaniroda Rendszere (http://valamilyen-iroda.hu)
  
Ha ezt a mezőt mégis elhagyod, `400 Bad Request` választ fogsz kapni.

## Adatformátumok

Az adatok betöltéséhez előtte azokat szerializálnod kell. A költözzbe API az XML és JSON szerializációt támogatja. Lekérdezéskor és betöltéskor a meghívott URL végén lévő kiterjesztéssel kell jelezned, melyik formátumot használod. Ha a kiterjesztés nem `xml` vagy `json`, akkor hibás URL miatt `404 Not Found` választ fogsz kapni.

[Az adatformátumokról bővebben](https://github.com/koltozzbehu/api/tree/master/api/adatformatumok.md)

## Hibák kezelése

* Ha a költözzbe API esetleg nem működne, `500`-as hibát adunk vissza. A te dolgod erre figyelni, és ilyen esetben újra próbálkozni.
* Ha hibás a beküldött JSON vagy XML, akkor `406 Not Acceptable` fejléc mellett visszaadjuk a parser által generált hibát.


## Hirdetések kezelése
A hirdetéseket saját rendszeredben kiosztott egyedi azonosítójukkal kell betöltened, és ezzel is tudod őket módosítani/törölni. Fontos, hogy az egyedi azonosítód csak betűket és számokat, pontot, aláhúzást és kötőjelet tartalmazhat. Az alábbi példákban az azonosító értéke `123456`. Lekérdezni és betölteni is XML vagy JSON szerializációval teudsz.

A hirdetések szerializációs formátuma megegyezik az [XML betöltésnél részletezett formátummal](https://github.com/koltozzbehu/api/tree/master/xml_betoltes/).

### Hirdetések lekérése
* A `GET /v1/listings.xml` parancsal lekérdezheted az irodához tartozó hirdetések azonosítóit.

### Egy adott hirdetés lekérése
* A `GET /v1/listing/123456.xml` parancs visszaadja a kért azonosítójú hirdetést.

### Hirdetés létrehozása
* A `POST /v1/listing/123456.xml` parancsal tudsz hirdetést betölteni.
* A parancs `201 Created`-al tér vissza, ha sikerült beszúrni a hirdetést. 
* Ha a hirdetés adatai nem megfelelőek (pl. hiányoznak kötelező mezők) akkor `400 Bad Request` fejléc mellett visszaadjuk az adatokkal kapcsolatos hibákat.

### Hirdetés módosítása
* A `PUT /v1/listing/123456.xml` parancs módosítja a kért azonosítójú hirdetést. Az összes mezőt át kell ádnod, nem csak azokat, amiket éppen módosítasz.
* A parancs `200 OK`-val tér vissza, ha sikerült módosítani a hirdetést. 
* Ha nincs ilyen azonosítójú hirdetés, akkor `404 Not Found`-ot adunk vissza.
* Ha a hirdetés adatai nem megfelelőek (pl. hiányoznak kötelező mezők) akkor `400 Bad Request` fejléc mellett visszaadjuk az adatokkal kapcsolatos hibákat.

### Hirdetés törlése
* A `DELETE /listing/1.xml` parancs kiadásával tudod a hirdetést törölni. Sikeres törlés esetén `204 No Content`-et adunk vissza, ha nincs ilyen hirdetés akkor `404 Not Found`-ot.

## Időzítés
Ennek a real-time API használatának az a legnagyobb haszna, hogy a módosítások azonnal átvezethetők a költözzbe.hu-ra. Ennek ellenére ezeket a hívásokat nem szerencsés akkor elvégezned, amikor a módosítás éppen történik a saját rendszerben. Előfordulhat, hogy nem sikerül a hivás, és az is, hogy sokáig tart.

Mi azt javasoljuk, hogy a beküldendő API hívásokat tárold el egy táblában (például: parancs, url, xml, rögzítés ideje, beküldés ideje, sikeres volt-e a kérés) akkor, amikor az esemény bekövetkezik. Ezeket a tárolt utasításokat pedig a háttérben, időzítve küldd be hozzánk (mondjuk 5-10 percenként). Így ha nem sikerül egy hívás, akkor a következő körben újra próbálkozhatsz vele (a szkriptek egymásra futására vigyázz azért).

Fontos, hogy az API nem arra való, hogy az összes hirdetésedet betöltsd vele minden nap. Ha csak erre használnád, akkor jobban jársz ha inkább az [XML betöltést](https://github.com/koltozzbehu/api/tree/master/xml_betoltes) választod. Ebben az esetben csak karban kell tartanod egy megadott XML fájlt, amit mi naponta letöltünk és feldolgozunk. A te oldalon ez jóval egyszerűbb és kevesebb a hibalehetőség.

## Tesztelj a homokozóban
A fejlesztés megkönnyítése érdekében használhatod a `http://homokozo.koltozzbe.hu/v1` url-t a teszthívásaiddal.
* Az ide küldött kérések nem módosítják az adatbázisunkat, de pontosan azt adják vissza, amit az éles API adna.
* Ezzel könnyen le tudod tesztelni, hogy az összeállított `xml` vagy `json` megfelelő-e.
* A homokozóba az `abcd1234abcd1234abcd1234abcd1234` iroda kulcsal is be tudsz jelentkezni, ha még nincs fiókod, és csak tesztelsz..

## Példák
* [PHP példa](https://github.com/koltozzbehu/api/tree/master/api/php_pelda.md)