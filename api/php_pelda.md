# PHP példa az API használatára

Ha PHP-val szeretnéd használi az API-t, akkor vagy shellből kell meghívnod a curl parancsokat, vagy használhatod a [PHP cURL függvényeit](http://php.net/manual/en/ref.curl.php). Mi ezutóbbit ajánljuk, az alábbi példák is ezt használják. Használhatsz JSON vagy XML szerializációt is. Mi a példában JSON-t használunk, mert erre van a PHP-nek beépített függvénye. Ha XML-t szeretnél használni, akkor az XML string elkészítését a hirdetés adataiból neked kell megoldanod.


## Hirdetés feladása

```php
$listing = array(
	'listing'=>array(
		'location'=>array(
			'city'=>'Eger',
		),
		'listing_details'=>array(
			'price'=>15000000,
			'listing_type'=>'eladó',
		),
		'basic_details'=>array(
			'property_type'=>'téglalakás',
			'room_count'=>2,
			'living_area'=>50
		),
	)
);

$listing_id = 'U123456'; // a hirdetés azonosítója a te rendszeredben

$curl = curl_init();

/* minden kérésnél ugyanazok a paraméterek */
curl_setopt($curl, CURLOPT_USERAGENT, "Irodád Rendszerének Neve (iroda-neve.hu)"); 
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HTTPAUTH,CURLAUTH_BASIC);
curl_setopt($curl, CURLOPT_USERPWD, 'abcd1234abcd1234abcd1234abcd1234:x');

/* kérésenként változó paraméterek */
curl_setopt($curl, CURLOPT_URL, "https://api.koltozzbe.hu/v1/listing/".$listing_id.".json"); 
curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($listing));

$response = curl_exec($curl);
$status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

if ($status_code=="201") {
	// sikerült létrehozni a hirdetést
} elseif ($status_code=="406") {
	// hibás json szerializáció
	echo $response; // ez a hiba
} else {
    // nem sikerült létrehozni
    echo $response; // ez az oka, hogy miért nem
}
curl_close($curl);
```

## Hirdetés módosítása

```php
$listing = array(/* ugyanaz mint fentebb */);

$listing_id = 'U123456'; // a hirdetés azonosítója a te rendszeredben

$curl = curl_init();

/* minden kérésnél ugyanazok a paraméterek */
curl_setopt($curl, CURLOPT_USERAGENT, "Irodád Rendszerének Neve (iroda-neve.hu)"); 
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HTTPAUTH,CURLAUTH_BASIC);
curl_setopt($curl, CURLOPT_USERPWD, 'abcd1234abcd1234abcd1234abcd1234:x');

/* kérésenként változó paraméterek */
curl_setopt($curl, CURLOPT_URL, "https://api.koltozzbe.hu/v1/listing/".$listing_id.".json");
curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($listing));

$response = curl_exec($curl);
$status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

if ($status_code=="200") {
	// sikerült módosítani a hirdetést
} elseif ($status_code=="404") {
	// nincs ilyen hirdetés
} elseif ($status_code=="406") {
	// hibás json szerializáció
	echo $response; // ez a hiba
} elseif ($status_code=="400") {
	// hibás hirdetés adatok, részletek a response body-ban
	echo $response; // ez a hiba
}

curl_close($curl);
```



## Hirdetés törlése

```php
$curl = curl_init();

$listing_id = 'U123456'; // a hirdetés azonosítója a te rendszeredben

/* minden kérésnél ugyanazok a paraméterek */
curl_setopt($curl, CURLOPT_USERAGENT, "Irodád Rendszerének Neve (iroda-neve.hu)"); 
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HTTPAUTH,CURLAUTH_BASIC);
curl_setopt($curl, CURLOPT_USERPWD, 'abcd1234abcd1234abcd1234abcd1234:x');

/* kérésenként változó paraméterek */
curl_setopt($curl, CURLOPT_URL, "https://api.koltozzbe.hu/v1/listing/".$listing_id.".json");
curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");

$response = curl_exec($curl);
$status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

if ($status_code=="204") {
	// sikerült törölni a hirdetést
} elseif ($status_code=="404") {
	// nincs ilyen hirdetés
}

curl_close($curl);
```

