# Különbségek az XML és a JSON formátumok között

A költözzbe API-t úgy készítettük el, hogy szabadon választhass a szerializációs formátumok között. Éppen ezért betöltéskor és lekérdezéskor is választhatsz, hogy XML-ben vagy JSON-ben szeretnéd-e az adatokat megadni vagy visszakapni.

Sajnos JSON-ben nem lehet egy mezőn belül ugyanazzal a kulcsal több érték, így az ilyen esetekben eltér a két a formátum.

## Egy egyszerű példa XML-ben

```xml
<listing>
	<location>
		<city>Eger</city>
	</location>
	<listing_details>
		<price>10000000</price>
		<listing_type>eladó</listing_type>
	</listing_details>
	<basic_details>
		<property_type>Téglalakás</property_type>
		<room_count>2</room_count>
		<living_area>50</living_area> 	
	</basic_details>
	<pictures>
		<picture>
			<picture_url>http://iroda.hu/kep1.jpg</picture_url>
		</picture>
		<picture>
			<picture_url>http://iroda.hu/kep2.jpg</picture_url>
		</picture>		
	</pictures>
</listing>
```

## Ugyanez JSON-ben

```json
{
	"listing":{
		"location":{
			"city":"Eger"
		},
		"listing_details":{
			"price":"10000000",
			"listing_type":"elad\u00f3"
		},
		"basic_details":{
			"property_type":"T\u00e9glalak\u00e1s",
			"room_count":"2",
			"living_area":"50"
		},
		"pictures":{
			"picture":[
				{
					"picture_url":"http:\/\/iroda.hu\/kep1.jpg"
				},
				{
					"picture_url":"http:\/\/iroda.hu\/kep2.jpg"
				}
			]
		}
	}
}
```

## És ahogy ezt például PHP-ból érdemes generálni

```php
echo json_encode(array(
	'listing'=>array(
		'location'=>array(
			'city'=>'Eger',
		),
		'listing_details'=>array(
			'price'=>'10000000',
			'listing_type'=>'eladó'
		),
		'basic_details'=>array(
			'property_type'=>'Téglalakás',
			'room_count'=>'2',
			'living_area'=>'50',
		),
		'pictures'=>array(
			'picture'=>array(
				array(
					'picture_url'=>'http://iroda.hu/kep1.jpg',
				),
				array(
					'picture_url'=>'http://iroda.hu/kep2.jpg',
				),
			),
		),
	),
));
```