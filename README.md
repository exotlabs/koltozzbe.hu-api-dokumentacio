# Ingatlanhirdetések betöltése a költözzbe.hu adatbázisába

Kétféleképp töltheted be irodád hirdetéseit adatbázisunkba. Az egyszerűbb és gyorsabb mód az, hogy létrehozol egy, a specifikációnkak megfelelő XML adatfájlt, amit saját weboldaladon elérhetővé teszel számunkra, és megmondod, hogy hol keressük (azaz megadod az URL-jét). Ezt a fájlt mi naponta (legalább) egyszer letöltjük és feldolgozzuk. Az XML-ben szereplő hirdetéseket beszúrjuk (ha pedig korábban már beszúrtuk, akkor frissítjük), az XML-ből hiányzó, régebben betöltött hirdetéseket pedig töröljük.

A második, kicsit bonyolultabb mód a költözzbe API-n keresztüli betöltés. Ilyenkor ugyanúgy az XML specifikáció szerint kell összeállítanod a hirdetések adatait, ám azokat egyenként, egy RESTful interfészen keresztül tudod betölteni. Ezzel lehetővé válik, hogy saját rendszeredet úgy kösd össze a mi adatbázisunkkal, hogy a nálad elvégzett módosítások azonnal átkerüljenek hozzánk is.

### Regisztráció a költözzbe.hu-n

Bármelyik megoldást választod is, első lépésként mindenképpen regisztrálnod kell irodádat (és irodavezetői fiókodat) nálunk. Csak a regisztráció után tudod fiókodban beállítani, hogyan szeretnéd hirdetéseidet betölteni.

[Regisztrálj most](http://koltozzbe.hu/ingatlanirodaknak/regisztracio/), ha még nem tetted volna meg.

### A referensek regisztrációja

A referenseid fiókját nem hozzuk létre. Akár XML adatfájlon, akár az API-n keresztül töltöd be a hirdetéseidet, a hirdetésekhez megadott referens adatokat egyszerűen csak eltároljuk és a hirdetés adatlapján megjelenítjük, a referens fiókja viszont nem jön létre.

Ha a referenseid szeretnék látni saját hirdetéseiket és azok statisztikáit, akkor maguknak kell regisztrálniuk a költözzbe.hu-n (küldhetsz nekik emailben meghívót a profilodból, hogy gyorsabban menjen).

Fontos: A referensek azzal az email címmel regisztráljanak, amit a hozzájuk tartozó hirdetéseknél megadtatok! Ilyenkor ugyanis ezek a hirdetések automatikusan a referens fiókjába kerülnek.

## XML betöltés: az egyszerű mód

Ha nincs szükséged arra, hogy módosításaid azonnal átkerüljenek a mi adatbázisunkba is, akkor mindenképpen válaszd az egyszerű XML betöltést. 

* [XML betöltés leírása](https://github.com/koltozzbehu/api/tree/master/xml_betoltes)

## Költözzbe API-n keresztüli betöltés: a real-time mód

A költözzbe API-val egyenként töltheted be (és módosíthatod, törölheted) hirdetéseidet saját rendszeredből. 

* [API-n keresztüli betöltés leírása](https://github.com/koltozzbehu/api/tree/master/api)



