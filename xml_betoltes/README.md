# Egyszerű XML betöltés

Az egyszerű XML betöltésnél csak össze kell állítanod egy teljes listát az összes hirdetésedről és azt elérhetővé kell tenned a weben. Ezt mi (legalább) egyszer egy nap letöltjük és feldolgozzuk. Az új hirdetéseket beszúrjuk, a meglévőket frissítjük (ha kell), a már eltűnteket pedig töröljük.

Ha nem szeretnéd hogy ezt a file-t más is elérhesse, használhatsz HTTP Basic Authentikációt. Ebben az esetben az URL előtt add meg a felhasználónév/jelszó párost így: `http://usernev:jelszo@ingatlaniroda.hu/ingatlanok.xml`

Ha egy hirdetéshez átadod a hozzá tartozó referens adatait, akkor a hirdetés adatlapján mi megjelenítjük azokat. Ha nem adod át, az irodához megadott adatok fognak megjelenni.

Mi azt javasoljuk, hogy minden egyes mező értékét `<![CDATA[` és `]]>` között küldd át, így egészen biztosan nem lesz gond az XML értelmezésével. Mi az alábbi példákból - a könnyebb emészthetőség érdekében - ezt most elhagytuk.

A végeredmény tehát így kell, hogy kinézzen:


## Egy nagyon egyszerű példa

```xml
<listing>
	<listing_id>123456</listing_id> <!-- a hirdetés távoli (azaz a te rendszeredben kapott) egyedi azonosítója, ügyelj rá, hogy ez egy adott hirdetésnél soha ne változzon! -->
	<internal_id>LAK123</internal_id> <!-- az ebben a mezőben átadott azonosítót jelenítjük meg a hirdetéseid listájában, és erre tudsz keresni is. Azt az azonosítót érdemes itt megadnod, ami alapján keresnéd a hirdetést. Ez lehet ugyanaz mint, a listing_id, de különbözhet is. -->
	<location>
		<city>Debrecen</city>
	</location>
	<listing_details>
		<price>10000000</price>
		<listing_type>eladó</listing_type>
	</listing_details>
	<basic_details>
		<property_type>Téglalakás</property_type>
		<room_count>2</room_count>
		<living_area>50</living_area> 	
	</basic_details>
</listing>

```

## Egy összetettebb példa

```xml
<listing>
	<listing_id>123456</listing_id>
	<agent_contact>
		<first_name>Péter</first_name>
		<last_name>Kovács</last_name>
		<email_address>peter.kovacs@koltozzbe.hu</email_address>
		<phone_number>+36704541234</phone_number>
	</agent_contact>
	<location>
		<city>Budapest</city>
		<district>VII. kerület</district>
		<suburb>Külső Erzsébetváros</suburb>
		<street>Alsó Erdősor utca</street>
		<floor>1. emelet</floor>
	</location>
	<listing_details>
		<price>10000000</price>
		<listing_type>eladó</listing_type>
	</listing_details>
	<basic_details>
		<property_type>Téglalakás</property_type>
		<room_count>1</room_count>
		<half_room_count>1</half_room_count>
		<bathroom_count>1</bathroom_count>
		<living_area>50</living_area> 	
		<year_built>1925</year_built>
	</basic_details>
	<pictures>
		<picture>
			<picture_url>http://ingatlaniroda.hu/kepek/123456-1.jpg</picture_url>
			<caption>A nappali és az előszoba</caption>
		</picture>
		<picture>
			<picture_url>http://ingatlaniroda.hu/kepek/123456-2.jpg</picture_url>
			<caption>Kilátás az utcára</caption>
		</picture>		
	</pictures>
	<rich_details>
		<stories_in_building>4</stories_in_building>
		<property_state>Felújított</property_state>
		<utilities>
			<utility_type>Víz</utility_type>
			<utility_type>Gáz</utility_type>
			<utility_type>Villany</utility_type>
			<utility_type>Csatorna</utility_type>
			<utility_type>Szélessáv</utility_type>
		</utilities>
		
		<parking>
			<parking_type>Utcán</parking_type>
		</parking>
		
		<heating>
			<heating_type>Gázcirkó</heating_type>
		</heating> 
				
		<appliances>
			<appliance_type>Sütő</appliance_type>
			<appliance_type>Hűtőgép</appliance_type>
		</appliances>
		
		<viewtypes>
			<view_type>Utcai</view_type>
		</viewtypes>
	
		<rooms>
			<room>
				<type>Nappali</type>
				<size>18</size>
				<orientation>Nyugati</orientation>
				<floor_type>Laminált padló</floor_type>
			</room>
			<room>
				<type>Szoba</type>
				<size>10</size>
				<orientation>Déli</orientation>
				<floor_type>Laminált padló</floor_type>
			</room>
			<room>
				<type>Konyha</type>
				<size>10</size>
				<floor_type>Járólap</floor_type>
			</room>
		</rooms>
	
	</rich_details>
	
</listing>

```

## XML adatfájl készítésénél így tehetsz több hirdetést egy fájlba

```xml
<listings>
	<listing>
		<listing_id>123456</listing_id>
		<location></location>
		...
	</listing>
	
	<listing></listing>
	<listing></listing>
	...
	...
</listings>
```


# A listing elem lehetésges mezői
A hirdetés adatait az alábbiak szerint kell összeállítanod.
```xml
<listing>
	<agent_contact>...</agent_contact> <!-- a referens adatai, nem kötelező -->
	<location>...</location> <!-- az ingatlan földrajzi helyetét leíró mezők -->
	<listing_details>...</listing_details> <!-- a hirdetés alapvető adatai -->
	<basic_details>...</basic_details> <!-- az ingatlan alapvető adatai -->
	<pictures>...</pictures> <!-- a fényképek listája, nem kötelező -->
	<rich_details>...</rich_details> <!-- az ingatlan extra adatai, nem kötelező -->
</listing>
```

### agent_contact
Ebben az elemben adhatod meg a hirdetéshez tartozó referens adatait. Ha elhagyod, a hirdetés közvetlenül az iroda alá kerül, referens nélkül. A hirdetés adatlapján ebben az esetben az iroda adatai (név, telefon, email) jelenik majd meg.
```xml
<agent_contact>
	<first_name>Péter</first_name> <!-- keresztnév -->
	<last_name>Kovács</last_name> <!-- családi név -->
	<email_address>peter.kovacs@koltozzbe.hu</email_address> <!-- FONTOS: Az itt megadott email cím alapján kötjük ezt a hirdetést a referensed fiókjához. Ha nem egyezik a két cím, nem látja majd a hirdetést! -->
	<phone_number>+36704541234</phone_number> <!-- telefonszám, a formátum is fontos -->
</agent_contact>
```

### location
Ebben az elemben a meghirdetett ingatlan elhelyezkedését kell megadnod. Településnek minden olyan települést elfogadunk, amely szerepel a [Magyar Posta irányítószám adatbázisában](http://www.posta.hu/ugyfelszolgalat/iranyitoszam_kereso). Fontos viszont, hogy településrészeket ne adj át (azaz "Gárdony-Agárd" helyett "Gárdony"-t kell megadnod). A jelenleg elfogadott városrészek listáját itt találod: [varosreszek.md](https://github.com/koltozzbehu/api/tree/master/xml_betoltes/varosreszek.md). Ha utcát is adsz át, azt is a posta adatbázisában szereplő alakjában add meg.

Ha megadod a koordinátákat, térkép is lesz a hirdetéshez. A koordinátákat WGS84-ben kell megadnod. Fontos, hogy ha megadod a koordinátákat, de nem adsz `meg map_radius_in_meters`-t (azaz a pontos helyet adod meg), akkor a koordináták alapján megpróbáljuk kitalálni a városrész mező értékét, felülírva ezzel a `suburb` mezőben átadott értéket.
```xml
<location>
	<zip>1074</zip> <!-- irányítószám, nem kötelező -->
	<city>Budapest</city> <!-- város -->
	<district>VII. kerület</district> <!-- kerület, Budapesten kötelező -->
	<suburb>Külső Erzsébetváros</suburb> <!-- városrész, nem kötelező -->
	<street>Alsó Erdősor utca</street> <!-- közterület neve és jellege, nem kötelező -->
	<unit_number>3</unit_number> <!-- házszám, nem kötelező -->
	<building>d</building> <!-- épület, nem kötelező -->
	<staircase>2</staircase> <!-- lépcsőház, nem kötelező -->
	<floor>1. emelet</floor> <!-- emelet, nem kötelező, lehetséges értékek: 1-20. emelet, Szuterén, Földszint, Félemelet, Magasföldszint, Tetőtér, -1. szint, -2. szint, -3. szint -->
	<door>24</door> <!-- ajtó, nem kötelező -->
	<coordinate_lat>47.500141</coordinate_lat> <!-- WGS84 koordináta, nem kötelező -->
	<coordinate_lng>19.075161</coordinate_lng> <!-- WGS84 koordináta, nem kötelező -->
	<map_radius_in_meters></map_radius_in_meters> <!-- a térképen megjelenített kör sugara méterben, nem kötelező. Ha megadod, egy kör jelenik meg a térképen, ha nem, akkor egy gombostű jelöli a pontos helyet. Lehetséges értékek: 50, 250, 500 -->
</location>
```

### listing_details
Ebben az elemben a hirdetés alapadatait kell megadnod. Egy hirdetés nem lehet egyszerre eladó és kiadó, ilyen esetben azt kétszer kell feltöltened.
```xml
<listing_details>
	<price>22000000</price> <!-- a hirdetett ingatlan ára -->
	<listing_type>eladó</listing_type> <!-- eladó vagy kiadó -->
</listing_details>
```

### basic_details
Ebben az elemben az ingatlan alapvető adatait kell megadnod
```xml
<basic_details>
	<property_type>téglalakás</property_type> <!-- tipus (kategória), lehetséges értékek: családi ház, ikerház, sorház, panellakás, téglalakás, penthouse, telek, garázs, nyaraló, iroda, üzlethelyiség, ipari, mezőgazdasági, hotel -->
	<description>Eladó lakás az Alsó Erdősor utcában</description> <!-- szöveges leírás (a html-t eldobjuk, kivéve a br-t), nem kötelező -->
	<room_count>2</room_count> <!-- szobák száma, nem kötelező -->
	<half_room_count>1</half_room_count> <!-- félszobák száma, nem kötelező -->
	<bathroom_count>1</bathroom_count> <!-- fürdőszobák száma, nem kötelező -->
	<living_area>78</living_area> <!-- lakóterület, nem kötelező -->
	<lot_size></lot_size> <!-- telekméret, nem kötelező -->
	<year_built>1918</year_built> <!-- építés éve, nem kötelező -->
</basic_details>
```

### pictures
Ebben a mezőben adhatod át a képeket. A fotók mindenki számára elérhető URL-jét kell megadnod, és átadhatdsz képleírást is. Egyetlen kép megadása sem kötelező, és felső határ sincs.
```xml
<pictures> <!-- ebből csak egy kell -->
	<picture> <!-- ebből pedig minden képhez egy -->
		<picture_url>http://iroda.hu/kep1.jpg</picture_url> <!-- a kép URL-je -->
		<caption>A nappali és a konyha</caption> <!-- és a leírása -->
	</picture>
	<picture> .. </picture>
	<picture> .. </picture>
	...
	...
</pictures>
```
		
### rich_details
Ebben a mezőben opcionális, az ingatlant pontosan leíró adatokat adhatsz meg. Minél részletesebben töltöd ki őket, annál könnyebben találják majd meg lakáskeresőink a hirdetéseidet.
```xml
<rich_details>
	<stories>1</stories> <!-- szintek száma az ingatlan belül (lakásoknál általában 1, családi házaknál 1,2,3, duplex lakásoknál 2) -->
	<stories_in_building>4</stories_in_building> <!-- szintek száma az egész épületben (1, ha földszintes és pl. 8, ha hét emeletes) -->
	<parking_spaces></parking_spaces> <!-- parkolóhelyek száma (nem az egész lakóparkban, hanem csak ehhez a lakáshoz (0: nincs, 1 felett: a parkolóhelyek száma) -->
	<doorman>0</doorman> <!-- portaszolgálat (0 nincs, 1 van) -->
	<elevator>0</elevator> <!-- lift (0 nincs, 1 van) -->
	<swimming_pool>0</swimming_pool> <!-- uszoda vagy úszómedence (0 nincs, 1 van) -->
	<jacuzzi>0</jacuzzi> <!-- jacuzzi (0 nincs, 1 van) -->
	<sauna>0</sauna> <!-- szauna(0 nincs, 1 van) -->
	<fitness>0</fitness> <!-- fitness szoba/terem (0 nincs, 1 van) -->
	
	<property_state>átlagos</property_state> <!-- ingatlan állapota, lehetséges értékek: felújított, felújítandó, átlagos, luxus, újszerű, jó -->
	
	<area_type>lakóövezet</area_type> <!-- övezet, lehetséges értékek: lakóövezet, üdülőövezet, külterület-->
	
	<utilities> <!-- közművek, több értéket is megadhatsz -->
		<utility_type>víz</utility_type>
		<utility_type>gáz</utility_type>
		<!-- lehetséges értékek: víz, gáz, villany, csatorna, szélessáv, kábeltv -->
		...
	</utilities>
	
	<parking> <!-- parkolás, több értéket is megadhatsz -->
		<parking_type>utcán</parking_type>
		<!-- lehetséges értékek: utcán, garázs, kocsibeálló, parkolóház, nincs parkoló -->
		...
	</parking>
	
	<heating> <!-- fűtés, több értéket is megadhatsz -->
		<heating_type>gázcirkó</heating_type>
		<!-- lehetséges értékek: gázcirkó, gázkonvektor, héra, távfűtés, elektromos, házközponti, házközponti egyedi méréssel, geotermikus, egyéb -->
		...	
	</heating> 
	
	<cooling> <!-- hűtés, több értéket is megadhatsz -->
		<cooling_type>split klíma</cooling_type>
		<!-- lehetséges értékek: központi klíma, split klíma, geotermikus, egyéb -->
		...		
	</cooling>
	
	<appliances> <!-- beépített gépek, több értéket is megadhatsz -->
		<appliance_type>mosógép</appliance_type>
		<!-- lehetséges értékek: mosógép, mosogatógép, hűtőgép, mélyhűtő, mikró, sütő -->
		...		
	</appliances>
	
	<viewtypes> <!-- kilátás, több értéket is megadhatsz -->
		<view_type>városi panoráma</view_type>
		<!-- lehetséges értékek: udvari, utcai, kertre néz, hegyi panoráma, városi panoráma, vízi panoráma -->
		...		
	</viewtypes>

	<rooms> <!-- szobák részletes listája>
		<room> <!-- ahány szoba, annyi room elem -->
			<type>nappali</type> <!-- a szoba tipusa, lehetésges értékek: szoba, étkező, nappali, nappali + amerikai konyha, hálószoba, konyha, mosókonyha, könyvtár, fürdőszoba, wc, dolgozószoba, konditerem, tároló, télikert, közlekedő, egyéb, amerikai konyha, garázs, kocsibeálló, gardrób, előszoba, kamra, pince, borospince, fürdőszoba wc-vel, hall, háztartási helyiség, kazánház, lépcsőház, loggia, medencetér, mosdó, padlástér, szauna, üzlet, wc kézmosóval, zuhanyzó -->
			<size>20.5</size> <!-- a szoba alapterülete négyzetméterben (float), nem kötelező -->
			<description></description> <!-- a szoba leírása, nem kötelező -->
			<orientation>déli</orientation> <!-- a szoba fő tájolása (amerre a legnagyobb üvegfelület néz), nem kötelező, lehetséges értékek: északi, északkeleti, keleti, délkeleti, déli, délnyugati, nyugati, északnyugati -->
			<floor_type>laminált padló</floor_type> <!-- a szoba padlója, nem kötelező, lehetésges értékek: járólap, laminált padló, parketta, padlószőnyeg, linóleum, beton, egyéb, hajópadló -->
			<balcony_size>3</balcony_size> <!-- a szobából nyíló erkély vagy terasz mérete négyzetméterben (float) (egy teraszt csak egy szobához társíts akkor is, ha több szobából is ki lehet menni), nem kötelező -->
			<height>3.9</height> <!-- a szoba belmagassága méterben (float), nem kötelező -->	
		</room>
		...
		...
	</rooms>

</rich_details>
```



# Ellenőrizd az XML adatfájlt

Ha elkészültél, az URL megadásával ellenőrizheted, sikerült-e mindent helyesen átadnod.

* [XML ellenőrző eszköz](http://koltozzbe.hu/xml-ellenorzes/)
