# A betöltő által elfogadott városrészek listája





### Békéscsaba

* Almáskertek
* Almáskerti ipari terület
* Andrássy út-Bartók Béla út közötti városrész
* Békekertek
* Belváros
* Borjúréti kertek
* Déli iparterület
* Déli kisvárosias városrész
* Északi iparterület
* Északi kertvárosias városrész
* Északi kertvárosias városrész
* Jamina
* József Attila lakótelep
* Kastély szőlők
* Kazinczy lakótelep
* Keleti kertek
* Keleti kertvárosias városrész
* Mezőmegyer
* Milleniumi lakótelep
* Oncsakertek
* Penza lakótelep



### Cegléd

* Belváros
* Budai úti üdülő övezet
* Északi Ipari Övezet
* Északi lakótelep
* Keleti kertváros
* Kenderföld
* Nyugati kertváros
* Öregszőlő-kertváros
* Szűcstelep



### Debrecen

* Akadémiakert
* Auguszta
* Basahalom
* Belváros
* Biharikert
* Boldogfalvikert
* Burgundia
* Csapókert
* Csigekert
* Dobozikert
* Egyetemváros
* Epreskert
* Falóger
* Gerébytelep
* Gyógyszergyár
* Hatvan utcai kert
* Homokkert
* Hőforrás
* Ispotály
* József Attila-telep
* Júliatelep
* Kandia
* Kerekestelep
* Kertváros
* Kétútköz
* Kincseshegy
* Kondoros
* Köntösgát
* Köntöskert
* Köztemető
* Lencztelep
* Libakert
* Lóskúti
* Majorság
* Mesterfalva
* Miklóskapu
* Nagyerdő
* Nagyerdőalja
* Nagyerdőpark
* Nagyerdőtölgyes
* Nagysándortelep
* Nyulas
* Pércsikert
* Péterfia
* Postakert
* Sámsonikert
* Sestakert
* Shape 31
* Sóház
* Széchenyikert
* Szentlászlófalva
* Technikai park
* Téglagyár
* Tégláskert
* Tócóliget
* Tócórét
* Tócóskert
* Tócóvölgy
* Újkert
* Úrrétje
* Vargakert
* Vénkert
* Veres Péter-kert
* Wesselényi
* Zsibogó



### Dunakeszi

* Alag központ
* Alag-felső
* Alagi temető környéke
* Alagliget lakópark
* Barátság utcai lakótelep
* Malom árok lakópark
* MÁV telep
* Rév-dűlő
* Sporttelepek
* Szabadságliget
* Szent István park környéke
* Toldi lakópark
* Tóváros
* Városközpont, Tabán



### Eger

* Almagyar domb
* Belváros
* Belváros Kelet
* Cifra hóstya
* Csákó
* Déli ipartelep
* Felnémet
* Felsőváros
* Hajdúhegy
* Hatvani hóstya
* K2 és környéke
* Károlyváros
* Lajosváros Kelet
* Lajosváros Nyugat
* Maklári hóstya
* Rác hóstya
* Szent Miklós városrész
* Szépasszony-völgy
* Tihamér
* Vécsey-völgy



### Érd

* Érdliget - Kutyavár
* Mihálytelep
* Ófalu
* Parkváros
* Postástelep
* Tisztviselőtelep
* Tusculanum
* Újfalu
* Újtelep



### Gödöllő

* Alvég
* Antalhegy
* Anyaotthon környéke
* Belváros
* Blaha
* Boncsok
* Csanak
* Egyetemi városrész
* Erzsébet lakótelep
* Fenyves
* Haraszt
* Kazinczy utcai lakótelep
* Kecskés
* Kertváros
* Királytelep
* Klapka
* Máriabesnyő
* Máriabesnyő lakópark
* Marikatelep
* Nagyfenyves
* Palota kerti lakótelep
* Rőges
* Szent János utcai lakótelep
* Szőlő utcai lakótelep



### Győr

* Adyváros
* Bácsa
* Belváros
* Déli iparterület
* Góré-dűlő
* Gorkijváros
* Gyárváros
* Ipari park
* Janicsfalu
* József Attila lakótelep
* Kisbácsa
* Kismegyer
* Likócs
* Mákos-dűlő
* Marcaliváros I.
* Marcaliváros II.
* Nádorváros
* Pinnyéd
* Rábakert
* Raktárváros
* Révfalu
* Sárás
* Szabadhegy
* Sziget
* Újváros



### Hódmezővásárhely

* Béketelep
* Belváros
* Csúcs
* Hódtó
* Kertváros
* Kishomok
* Susán
* Tabán
* Tarján
* Újváros



### Budapest I. kerület

* Budavár
* Gellérthegy
* Krisztinaváros
* Tabán
* Víziváros



### Budapest II. kerület

* Adyliget
* Budaliget
* Csatárka
* Erzsébetliget
* Erzsébettelek
* Felhévíz
* Gercse
* Hársakalja
* Hárshegy
* Hűvösvölgy
* Kővár
* Kurucles
* Lipótmező
* Máriaremete
* Nyék
* Országút
* Pálvölgy
* Pasarét
* Pesthidegkút-ófalu
* Petneházyrét
* Remetekertváros
* Rézmál
* Rózsadomb
* Szemlőhegy
* Széphalom
* Szépilona
* Szépvölgy
* Törökvész
* Újlak
* Vérhalom
* Víziváros
* Zöldmál



### Budapest III. kerület

* Aquincum
* Aranyhegy
* Békásmegyer
* Csillaghegy
* Csúcshegy
* Filatorigát
* Hármashatárhegy
* Kaszásdűlő
* Mátyáshegy
* Mocsárosdűlő
* Óbuda
* Óbudaisziget
* Remetehegy
* Rómaifürdő
* Solymárvölgy
* Táborhegy
* Testvérhegy
* Törökkő
* Újlak
* Ürömhegy



### Budapest IV. kerület

* Istvántelek
* Káposztásmegyer
* Megyer
* Székesdűlő
* Újpest



### Budapest IX. kerület

* Ferencváros
* Gubacsidűlő
* József Attila lakótelep



### Kaposvár

* Belváros
* Deseda
* Donner - Rómahegy
* Északi városrész
* Északnyugati városrész
* Fészerlakpuszta
* Ivánfahegy, Kaposszentjakab
* Kecelhegy-Cser
* Kisgát és környéke, Cukorgyár, Pécsi utca és környéke
* Toponár
* Zselic kertváros



### Kecskemét

* Alsószéktó
* Árpád-város
* Belváros
* Bethlen-város
* Erzsébet-város
* Felsőszéktó
* Hunyadi-város
* Kossuth-város
* Mária-város
* Rákóczi-város
* Széchenyi-város
* Szent István-város
* Szent László város



### Miskolc

* Avas Dél városrész
* Avas Kelet városrész
* Avasalja
* Bábonyibérc
* Békeszálló telep
* Belváros
* Diósgyőr
* Egyetemváros
* Görömböly
* Győri kapu
* Hejőcsaba
* Keleti városrész
* Kilián
* Komlóstető
* Lyukóvölgy
* Martin kertváros
* Miskolc-Tapolca
* Pereces
* Selyemrét
* Szentpéteri kapu Kelet
* Szentpéteri kapu Nyugat
* Szirma
* Tetemvár
* Új Diósgyőr
* Vargahegy
* Vologda



### Nyíregyháza

* Belváros
* Borbánya
* Bujtos
* Déli ipartelep
* Érkert
* Hímes
* Huszártelep
* Jósaváros
* Kertváros
* Korányi kertváros
* Malomkert
* Nyugati iparterület
* Ókistelekiszőlő
* Oros
* Örökösföld
* Örökösföld kertvárosi része
* Sóstógyógyfürdő
* Sóstóhegy
* Stadion környéke
* Újkistelekiszőlő



### Ózd

* Béke lakótelep
* Bolyok
* Farkaslyuk
* Hódoscsépány
* Meggyes
* Ózd falu
* Ráctag
* Sajóvárkony
* Somsály
* Szenna
* Szentsimon
* Tábla
* Törzsgyár
* Uraj
* Városközpont
* Városközpont lakótelep



### Pécs

* Árpádváros
* Bálics
* Balokány
* Basamalom
* Belváros
* Budai külváros
* Csoronika
* Daindol
* Diós
* Donátus
* Északmegyer
* Füzes
* Gyárváros
* Gyükés
* Havihegy
* Hird
* Ispitaalja
* Kertváros
* Kovácstelep
* Makár
* Málom
* Mecsekoldal
* Megyer
* Meszesi lakóövezet
* Meszesi lakótelep
* Nagyárpád
* Patacs
* Pécsbánya
* Piricsizma
* Postavölgy
* Ráczváros
* Rigóder
* Rókusdomb
* Siklósi külváros
* Somogy
* Szabolcsfalu
* Szentkút
* Szentmiklós
* Szigeti külváros
* Szkókó
* Tüskésrét
* Újhegy
* Uránváros
* Ürög
* Vasas
* Zsebedomb



### Salgótarján

* Alsópálfalva
* Baglyasalja
* Béketelep
* Beszterce lakótelep
* Déli iparterület
* Eperjestelep
* Felsőpálfalva
* Forgáchtelep
* Gorkij lakótelep
* Idegértelep
* Jónástelep
* Katalintelep
* Kazárivölgy
* Kemerovó lakótelep
* Kempingtelep
* Kőváralja
* Művésztelep
* Pintértelep
* Somlyóbánya
* Szabadságtelep
* Városközpont
* Vizslás
* Zagyvapálfalva



### Sopron

* Ágfalvi úti lakótelep
* Alsólővérek
* Apácakert
* Aranyhegy
* Balfi külváros
* Bánfalva
* Bécsi külváros
* Belváros
* Deák tér környéke
* Északnyugati városrész
* Felsőlővérek
* Győri külváros
* Győti kapu
* Jereván lakótelep
* József Attila lakótelep
* Kőfaragó téri lakótelep
* Pihenőkereszt lakópark
* Újteleki külváros



### Szeged

* Alsóváros
* Baktói kiskertek
* Béke telep
* Belváros
* Dorozsma
* Északi város lakótelep
* Felsőváros
* Fodorkert
* Gyálarét
* Kálvária sugárút környéke
* Kecskés István telep
* Klebelsberg telep
* Makkosháza lakótelep
* Marostő
* Móraváros
* Odessza lakótelep
* Petőfitelep
* Rókus
* Szentmihály
* Szőreg
* Tápé
* Tarján lakótelep
* Tompa sziget
* Újrókus lakótelep
* Újszeged



### Székesfehérvár

* Almássy
* Alsóváros
* Belváros
* Búrtelep
* Fecskepart
* Feketehegy
* Felsőváros
* Horváth István lakótelep
* Kadocsa
* Királykút lakótelep
* Köfém
* Köfém lakótelep
* Lövölde utcai lakótelep
* Maroshegy
* Öreghegy
* Palotaváros
* Palotavárosi lakótelep
* Prohászka
* Rádiótelep
* Szedreskert
* Szedreskerti lakótelep
* Széna téri lakótelep
* Tóváros
* Vízivárosi lakótelep
* Ybl Miklós lakótelep



### Szekszárd

* Alsóváros
* Bakta
* Belváros
* Bottyán hegyi lakótelep
* Bottyánhegy
* Felsőváros
* Jókai lakótelep
* Kölcsey lakótelep
* Pollacky lakótelep
* Szőlőhegy
* Tartsay lakótelep
* Újváros
* Wosinszki lakótelep



### Szigetszentmiklós

* Bucka negyed
* Dunapart
* Felsőtag
* Városkörnyék
* Városközpont



### Szolnok

* Alcsi sziget
* Alcsi városrész
* Almási kertváros
* Belváros környéke
* Czakó kert
* Déli városrész
* Ferencváros
* József Attila úti lakótelep
* Kamaraváros
* Katonaváros
* Meggyesitelep
* Nagyváros
* Partoskápolna
* Pletykafalu
* Szandaszőlős
* Széchenyi lakótelep
* Széchenyi városrész
* Tallin lakótelep
* Tiszaliget
* Zagyvaparti lakótelep



### Szombathely

* Alsóhegy-Középhegy kertváros
* Belváros
* Belváros környéke
* Derkovits lakótelep
* Éhen Gyula kertváros
* Gyöngyöshermán kertváros
* Gyöngyösszőlős kertváros
* Herény
* Ifjúsági lakótelep
* Ipartelep
* Joskar-Ola lakótelep
* Kámon
* Oladi kertváros
* Oladi lakótelep
* Szentkirály kertváros
* Újperint



### Tatabánya

* 6-os telep
* Alsógalla
* Bánhida
* Bánhidai lakótelep
* Bárdos lakópark
* Cseri lakótelep
* Dózsakert
* Felsőgalla
* Gál István lakótelep
* Kertváros
* Kertvárosi lakótelep
* Mésztelep
* Ságvári Endre lakótelep
* Sárberek
* Sárberki lakótelep
* Újtelep
* Újváros lakótelep



### Budapest V. kerület

* Belváros
* Lipótváros



### Vác

* Alsóváros
* Alsóváros-Burgundia
* Alsóvárosi lakótelep
* Áltány
* Belváros
* Belváros-Tabán
* Deákvár-Bácska
* Deákvár-Kertváros
* Deákvár-Lajostelep
* Deákvár-Nyugat
* Deákvár-Óváros
* Deákvár-Papvölgy
* Deákvár-Szentmihály
* Deákvár-Téglaház
* Deákvár-Törökhegy
* Deákvári lakótelep
* Derecske
* Derecske-Liget
* Gombási úti lakótelep
* Hermány
* Kisvác
* Kisvác-Buki
* Kisvác-Iskolaváros
* Kisvác-Kőhíd
* Kisvác-Krakó
* Kisvác-Limbus
* Mária-liget
* Václiget



### Veszprém

* Bakonyalja
* Belváros
* Cholnokyváros
* Dózsaváros
* Egry József utcai lakótelep
* Egyetemváros
* Endrődi Sándor lakótelep
* Füredidomb
* Hóvirág lakótelep
* Iparváros
* Jeruzsálemhegy
* Jutasi úti lakótelep
* Jutaspuszta
* Nádortelep
* Pajtakert
* Takácskert
* Újtelep
* Veszprémvölgy



### Budapest VI. kerület

* Belső Terézváros
* Diplomatanegyed
* Külső Terézváros



### Budapest VII. kerület

* Belső Erzsébetváros
* Istvánmező
* Külső Erzsébetváros



### Budapest VIII. kerület

* Istvánmező
* Józsefváros
* Kerepesdűlő
* Palotanegyed
* Tisztviselőtelep



### Budapest X. kerület

* Felsőrákos
* Gyárdűlő
* Keresztúridűlő
* Kőbánya kertváros
* Kúttó
* Laposdűlő
* Ligettelek
* Népliget
* Óhegy
* Téglagyárdűlő
* Újhegy



### Budapest XI. kerület

* Albertfalva
* Dobogó
* Gazdagrét
* Gellérthegy
* Hosszúrét
* Kamaraerdő
* Kelenföld
* Kelenvölgy
* Kőérberek
* Lágymányos
* Madárhegy
* Nádorkert
* Őrmező
* Örsöd
* Péterhegy
* Pösingermajor
* Sasad
* Sashegy
* Spanyolrét



### Budapest XII. kerület

* Budakeszierdő
* Csillebérc
* Farkasrét
* Farkasvölgy
* Istenhegy
* Jánoshegy
* Kissvábhegy
* Krisztinaváros
* Kútvölgy
* Magasút
* Mártonhegy
* Németvölgy
* Orbánhegy
* Svábhegy
* Széchenyihegy
* Virányos
* Zugliget



### Budapest XIII. kerület

* Angyalföld
* Margitsziget
* Népsziget
* Újlipótváros
* Vizafogó



### Budapest XIV. kerület

* Alsórákos
* Herminamező
* Istvánmező
* Kiszugló
* Nagyzugló
* Rákosfalva
* Törökőr
* Városliget



### Budapest XIX. kerület

* Kispest
* Wekerletelep



### Budapest XV. kerület

* Pestújhely
* Rákospalota
* Újpalota



### Budapest XVI. kerület

* Árpádföld
* Cinkota
* Mátyásföld
* Rákosszentmihály
* Sashalom



### Budapest XVII. kerület

* Akadémiaújtelep
* Rákoscsaba
* Rákoscsaba-újtelep
* Rákoshegy
* Rákoskeresztúr
* Rákoskert
* Rákosliget



### Budapest XVIII. kerület

* Alacskai úti lakótelep
* Almáskert
* Bélatelep
* Belsőmajor
* Bókaykert
* Bókaytelep
* Erdőskert
* Erzsébettelep
* Ferihegy
* Ganzkertváros
* Ganztelep
* Gloriett-telep
* Halmierdő
* Havannatelep
* Kossuth Ferenc telep
* Lakatostelep
* Liptáktelep
* Lónyaytelep
* Miklóstelep
* Rendessytelep
* Szemeretelep
* Szent Imre kertváros
* Újpéteritelep



### Budapest XX. kerület

* Erzsébetfalva
* Gubacs
* Gubacsipuszta
* Kossuthfalva
* Pacsirtatelep
* Pesterzsébet-szabótelep



### Budapest XXI. kerület

* Csepel belváros
* Csepel kertváros
* Csepel rózsadomb
* Csepel-ófalu
* Csillagtelep
* Erdőalja
* Erdősor
* Gyártelep
* Háros
* Királyerdő
* Királymajor
* Szabótelep
* Szigetcsúcs



### Budapest XXII. kerület

* Baross Gábor telep
* Budafok
* Budatétény
* Nagytétény



### Budapest XXIII. kerület

* Gubacs
* Soroksár
* Soroksár-újtelep
