# Betöltés több irodát kezelő rendszereknek

Ha több ingatlaniroda megbízásait is kezeli rendszered, akkor a betöltésnél érdemes figyelni pár dologra.

## Ha XML betöltést szeretnél ügyfeleidnek
Biztosíts irodáidnak egy olyan XML fájlt, amit megadhat nálunk és amit karbantartasz. Ennek az URL-jét a te rendszeredből kell megtudnia, és nálunk, a fiókjában kell megadnia. 

Ezt a fájlt mi naponta (legalább) egyszer letöltjük és feldolgozzuk. Az XML-ben szereplő hirdetéseket beszúrjuk (ha pedig korábban már beszúrtuk, akkor frissítjük), az XML-ből hiányzó, régebben betöltött hirdetéseket pedig töröljük.

Fontos, hogy az XML URL-jét könnyen megtalálja a rendszeredben, és ne szoruljon segítségre az áttöltés beállításához.

## Ha API betöltést szeretnél ügyfeleidnek
Az irodához tartozó iroda kulcsot (agency_key-t) az irodavezető bármikor eléri fiókjában, ezt tőle kell beszereznetek, és eltárolnotok. Ennek birtokában tudsz betölteni hirdetéseket az iroda fiókjába. Mivel egy iroda több forrásból is tölthet be (például ha több ügyviteli rendszert használ), arra kérünk, hogy a hirdetés-azonosítókat úgy add át rendszeredből, hogy az ne ütközhessen egy másik rendszerből érkező azonosítóval. `100`-as azonosítójú hirdetés könnyen előfordulhat két rendszerben is, de ha rendszered nevét prefixként az azonosító elé illeszted (például `hazbank100`) akkor már nem fenyeget ez a veszély.

* [Az XML betöltés specifikációja](https://github.com/koltozzbehu/api/tree/master/xml_betoltes)
* [Az API specifikációja](https://github.com/koltozzbehu/api/tree/master/api/api)
* [Egyszerű PHP példa az API használatára](https://github.com/koltozzbehu/api/tree/master/api/php_pelda.md)

